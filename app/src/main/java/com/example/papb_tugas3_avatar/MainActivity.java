package com.example.papb_tugas3_avatar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView bodyImage, hairImage, eyesImage, beardImage, eyebrowImage, moustacheImage;
    private CheckBox checkBoxHair, checkBoxBeard, checkBoxMoustache, checkBoxEyebrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.bodyImage = findViewById(R.id.bodyImage) ;
        this.hairImage = findViewById(R.id.hairImage) ;
        this.eyesImage = findViewById(R.id.eyesImage) ;
        this.beardImage = findViewById(R.id.beardImage) ;
        this.eyebrowImage = findViewById(R.id.eyebrowImage) ;
        this.moustacheImage  = findViewById(R.id.moustacheImage);

        this.checkBoxHair = findViewById(R.id.checkBoxHair);
        this.checkBoxBeard = findViewById(R.id.checkBoxBeard);
        this.checkBoxEyebrow = findViewById(R.id.checkBoxEyebrow);
        this.checkBoxMoustache = findViewById(R.id.checkBoxMoustache);

        //set bodyImage inistailly visible
        hairImage.setVisibility(View.INVISIBLE);
        eyebrowImage.setVisibility(View.INVISIBLE);
        moustacheImage.setVisibility(View.INVISIBLE);
        beardImage.setVisibility(View.INVISIBLE);

        checkBoxHair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxHair.isChecked()){
                    hairImage.setVisibility(View.VISIBLE);
                } else{
                    hairImage.setVisibility(View.INVISIBLE);
                }
            }
        });

        checkBoxMoustache.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxMoustache.isChecked()){
                    moustacheImage.setVisibility(View.VISIBLE);
                } else{
                    moustacheImage.setVisibility(View.INVISIBLE);
                }
            }
        });

        checkBoxBeard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxBeard.isChecked()){
                    beardImage.setVisibility(View.VISIBLE);
                } else{
                    beardImage.setVisibility(View.INVISIBLE);
                }
            }
        });

        checkBoxEyebrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBoxEyebrow.isChecked()){
                    eyebrowImage.setVisibility(View.VISIBLE);
                } else{
                    eyebrowImage.setVisibility(View.INVISIBLE);
                }
            }
        });

    }
}